﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace DilerMotor
{
    public partial class FormAcceptRequest : Form
    {
        public OracleConnection conn;
        OracleCommand cmd = new OracleCommand();
        OracleTransaction myTrans;
        DataTable dtPembanding = new DataTable();
        string id = "";
        public FormAcceptRequest()
        {
            InitializeComponent();
            label2.Text = "Daftar Request";
            label3.Text = "Detail Request";
            label5.Text = "Daftar Barang";
            conn = FormLogin.conn;
            refreshGrid();
            gridBarang();
            getPembanding();
        }

        public FormUtama parent;
        private void FormAcceptRequest_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            parent.showFormMenuAdmin();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            dateLabel.Text = DateTime.Now.ToLongDateString();
            label1.Text = DateTime.Now.ToLongTimeString();
        }

        public void refreshGrid()
        {
            string query = "select * from hLogBrg where status_brg = 'R'";
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void getPembanding()
        {
            string query = "select * from barang";
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                oda.Fill(dtPembanding);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void detailGrid(string id)
        {
            string query = "select * from dLogBrg where id_hlog = '" + id + "'";
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView2.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void gridBarang()
        {
            string query = "select * from barang";
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView3.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show(dataGridView2[0, e.RowIndex].Value.ToString());
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show(e.RowIndex + "");
            //MessageBox.Show(dataGridView1[0, e.RowIndex].Value.ToString());
            id = dataGridView1[0, e.RowIndex].Value.ToString();
            detailGrid(id);
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string komentar = textBox1.Text;
            DialogResult konfirmasi = MessageBox.Show("Yakin Tolak?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (konfirmasi == DialogResult.Yes)
            {
                if (komentar.Length > 0 && id != "")
                {
                    string query = "update hLogBrg set status_brg = 'D', keterangan = '" + komentar + "' " + "where id_hlog = '" + id + "'";
                    cmd.Transaction = myTrans;
                    myTrans = conn.BeginTransaction();
                    cmd.Connection = conn;
                    try
                    {
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                        myTrans.Commit();
                        textBox1.Text = "";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        myTrans.Rollback();
                        }
                    }
                else MessageBox.Show("Keterangan tidak boleh kosong");
            }
            refreshGrid();
            gridBarang();
            getPembanding();
            dataGridView2.DataSource = null;
            
        }
        public string komentar = "-";
        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Yakin Terima?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (konfirmasi == DialogResult.Yes)
            {
                bool logic = true;
                DataTable dtBaru = new DataTable();
                string query = "select * from dLogBrg where id_hlog = '" + id + "'";
                try
                {
                    OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                    oda.Fill(dtBaru);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                if (id != "")
                {
                    for (int i = 0; i < dtBaru.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtPembanding.Rows.Count; j++)
                        {
                            //MessageBox.Show(dtBaru.Rows[i][1].ToString().Length + " || "+dtPembanding.Rows[j][0].ToString().Length);
                            if (dtBaru.Rows[i][1].ToString() == dtPembanding.Rows[j][0].ToString())//dt pembanding barang
                            {
                                if (Convert.ToInt32(dtBaru.Rows[i][2].ToString()) > Convert.ToInt32(dtPembanding.Rows[j][5].ToString()))
                                {
                                    MessageBox.Show("Stok " + dtPembanding.Rows[j][1].ToString() + " Tidak Mencukupi");
                                    logic = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (logic && id != "")
                    {
                        string query1 = "update hLogBrg set status_brg = 'A', keterangan = '" + "permintaan diterima. " + (textBox1.Text == "" ? komentar : textBox1.Text) + "' " + "where id_hlog = '" + id + "'";
                        //MessageBox.Show(query1);
                        cmd.Transaction = myTrans;
                        myTrans = conn.BeginTransaction();
                        cmd.Connection = conn;
                        try
                        {
                            cmd.CommandText = query1;
                            cmd.ExecuteNonQuery();
                            myTrans.Commit();
                            textBox1.Text = "";
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            myTrans.Rollback();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Mohon memilih request terlebih dahulu");
                }
            }
            refreshGrid();
            gridBarang();
            getPembanding();
            dataGridView2.DataSource = null;
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
