﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace DilerMotor
{
    public partial class FormDaftarRequest : Form
    {
        OracleConnection conn = FormLogin.conn;
        public FormDaftarRequest()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showFormRequestBarangSales();
        }

        public FormUtama parent;
        private void FormDaftarRequest_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            if(FormUtama.server == "DPS")
            {
                label2.Text = "Denpasar";
                loadDenpasar();
            }
            else
            {
                label2.Text = "Surabaya";
                loadSurabaya();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void loadSurabaya()
        {
            string query = "select * from request where tujuan = 'SBY'";
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loadDenpasar()
        {
            string query = "select * from request where tujuan = 'DPS'";
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string id = dataGridView1[0, e.RowIndex].Value.ToString();
            detailGrid(id);
        }

        public void detailGrid(string id)
        {
            string query = "select d.id_brg,b.nama_brg,d.qty from drequest d,barang b where d.id_hlog = '" + id + "' and d.id_brg = b.id_brg";
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView2.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
