﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace DilerMotor
{
    public partial class FormDaftarTrans : Form
    {
        OracleConnection conn = FormLogin.conn;
        public FormDaftarTrans()
        {
            InitializeComponent();
            //ini bisa diliat gudang dan dealer
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
            if (FormUtama.server == "DPS" || FormUtama.server == "SBY")
            {
                parent.showFormRequestBarangSales();
            }
            else
            {
                parent.showFormMenuAdmin();
            }
        }

        public FormUtama parent;
        private void FormDaftarTrans_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            if(FormUtama.server == "SBY")
            {
                button1.Visible = false;
                button2.Visible = false;
                loadSurabaya();
            }
            else if(FormUtama.server == "DPS")
            {
                label1.Text = "Denpasar";
                loadDenpasar();
            }
            else
            {
                loadSurabaya();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadSurabaya();
        }

        private void loadSurabaya()
        {
            string query;
            dataGridView2.DataSource = null;
            if (FormUtama.server == "DPS" || FormUtama.server == "SBY") 
            {
                query = "select * from hTrans";
            }
            else
            {
                query = "select * from hTransSBY";
            }
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
                label1.Text = "Surabaya";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loadDenpasar()
        {
            dataGridView2.DataSource = null;
            string query;
            if (FormUtama.server == "DPS" || FormUtama.server == "SBY")
            {
                query = "select * from hTrans";
            }
            else
            {
                query = "select * from hTransDPS";
            }
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
                label1.Text = "Denpasar";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            loadDenpasar();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string id = dataGridView1[0, e.RowIndex].Value.ToString();
            detailGrid(id);
        }

        public void detailGrid(string id)
        {
            string query = "";
            if(label1.Text == "Surabaya")
            {
                if (FormUtama.server == "DPS" || FormUtama.server == "SBY")
                {
                    query = "select d.id_brg,b.nama_brg,d.qty,d.subtotal from dTrans d,barang b where d.idTrans = '" + id + "' and d.id_brg = b.id_brg";
                }
                else
                {
                    query = "select d.id_brg,b.nama_brg,d.qty,d.subtotal from dTransSBY d,barang b where d.idTrans = '" + id + "' and d.id_brg = b.id_brg";
                }
            }
            else
            {
                if (FormUtama.server == "DPS" || FormUtama.server == "SBY")
                {
                    query = "select d.id_brg,b.nama_brg,d.qty,d.subtotal from dTrans d,barang b where d.idTrans = '" + id + "' and d.id_brg = b.id_brg";
                }
                else
                {
                    query = "select d.id_brg,b.nama_brg,d.qty,d.subtotal from dTransDPS d,barang b where d.idTrans = '" + id + "' and d.id_brg = b.id_brg";
                }
            }
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView2.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
