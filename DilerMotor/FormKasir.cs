﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace DilerMotor
{
    public partial class FormKasir : Form
    {
        OracleConnection conn;
        OracleDataAdapter da = new OracleDataAdapter();
        OracleCommand cmd = new OracleCommand();
        OracleTransaction myTrans;
        Form parent;
        public FormKasir(OracleConnection conn, Form parent)
        {
            InitializeComponent();

            btnCheckout.Enabled = false;

            dateLabel.Text = DateTime.Now.ToLongDateString();
            timeLabel.Text = DateTime.Now.ToLongTimeString();
            this.parent = parent;
            this.conn = conn;
            addCMBMotor();
            setupDGVTrans();
        }

        public void setupDGVTrans()
        {
            dgvTrans.Columns.Add("Kode", "Kode");
            dgvTrans.Columns.Add("Nama", "Nama");
            dgvTrans.Columns.Add("Qty", "Qty");
            dgvTrans.Columns.Add("Harga", "Harga");
            dgvTrans.Columns.Add("Subtotal", "Subtotal");
            dgvTrans.Columns[0].Visible = false;
            DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
            btn.HeaderText = "Hapus";
            btn.Text = "Hapus";
            btn.Name = "Hapus";
            btn.UseColumnTextForButtonValue = true;
            dgvTrans.Columns.Add(btn);
        }

        public void addCMBMotor()
        {
            conn.Open();
            DataSet ds = new DataSet();
            string qry = "SELECT * FROM barang where stok>0";
            OracleDataAdapter da = new OracleDataAdapter(qry, conn);
            da.Fill(ds);
            cmbMotor.DataSource = ds.Tables[0];
            cmbMotor.ValueMember = "id_brg";
            cmbMotor.DisplayMember = "nama_brg";
            conn.Close();
        }
        int total;

        private void btnTambah_Click(object sender, EventArgs e)
        {
            conn.Open();
            cmd.Connection = conn;
            String id_brg = cmbMotor.SelectedValue.ToString();
            if (id_brg != "" && nmrQty.Value>0)
            {
                cmd.CommandText = "SELECT hrg_jual FROM barang where id_brg = '" + id_brg + "'";
                int harga = Convert.ToInt32(cmd.ExecuteScalar() + "");
                conn.Close();
                int subtotal = (int)nmrQty.Value * harga;

                int sama = 0;
                for (int i = 0; i < dgvTrans.Rows.Count; i++)
                {
                    if (dgvTrans.Rows[i].Cells[0].Value.ToString() == id_brg)
                    {
                        sama = 1;
                        dgvTrans.Rows[i].Cells[2].Value = nmrQty.Value;
                        dgvTrans.Rows[i].Cells[4].Value = subtotal;
                    }
                }
                if (sama == 0)
                {
                    dgvTrans.Rows.Add(id_brg, cmbMotor.Text, nmrQty.Value, harga, subtotal);
                }

                total = 0;
                for (int i = 0; i < dgvTrans.Rows.Count; i++)
                {
                    total += Convert.ToInt32(dgvTrans.Rows[i].Cells[4].Value.ToString());
                }

                lblTotal.Text = total.ToString();
            }
        }

        private void DateRun_Tick(object sender, EventArgs e)
        {
            dateLabel.Text = DateTime.Now.ToLongDateString();
            timeLabel.Text = DateTime.Now.ToLongTimeString();
        }

        private void dgvTrans_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                total -= Convert.ToInt32(dgvTrans[4, e.RowIndex].Value.ToString());
                dgvTrans.Rows.RemoveAt(e.RowIndex);
                lblTotal.Text = total + "";
            }
        }

        private void btnCheckout_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Yakin?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (konfirmasi == DialogResult.Yes)
            {
                if (dgvTrans.Rows.Count > 0)
                {
                    conn.Open();
                    cmd.Transaction = myTrans;
                    myTrans = conn.BeginTransaction();
                    cmd.Connection = conn;
                    try
                    {
                        string nomornota = autoGen();
                        string now = DateTime.Now.ToString("dd/MM/yyyy");
                        cmd.CommandText = "INSERT INTO HTRANS VALUES('" + nomornota + "',to_date('" + now + "','dd-mm-yyyy')," + total + ",'" + txtCustomer.Text + "','"+txtCatatan.Text+"')";
                        cmd.ExecuteNonQuery();
                        for (int i = 0; i < dgvTrans.Rows.Count; i++)
                        {
                            cmd.CommandText = "SELECT stok FROM barang where id_brg = '" + dgvTrans[0, i].Value.ToString() + "'";
                            int stok = Convert.ToInt32(cmd.ExecuteScalar() + "");
                            stok -= Convert.ToInt32(dgvTrans[2, i].Value.ToString());
                            cmd.CommandText = "Update barang set stok = " + stok + " where id_brg= '" + dgvTrans[0, i].Value.ToString() + "'";
                            cmd.ExecuteNonQuery();
                            cmd.CommandText = "INSERT INTO DTRANS VALUES('" + nomornota + "','" + dgvTrans[0, i].Value.ToString() + "'," + dgvTrans[2, i].Value.ToString() + "," + dgvTrans[4, i].Value.ToString() + ")";
                            cmd.ExecuteNonQuery();
                        }
                        myTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        myTrans.Rollback();
                    }
                    conn.Close();
                }
                total = 0;
                txtCatatan.Text = "";
                txtCustomer.Text = "";
                lblTotal.Text = "0";
                cmbMotor.SelectedIndex = 0;
                btnCheckout.Enabled = false;
                //nmrQty.Value = 0;
                dgvTrans.Rows.Clear();
            }
        }
        private String autoGen()
        {
            string now = DateTime.Now.ToString("ddMMyy");
            int angka = 0;
            cmd.CommandText = "Select nvl(max(substr(idTrans,8,3)),0)+1 from hTrans where substr(idTrans,1,7) = 'T" + now + "'";

            angka = Convert.ToInt32(cmd.ExecuteScalar() + "");
            String str_angka = Convert.ToString(angka);
            str_angka = str_angka.PadLeft(3, '0');
            return "T" + now + str_angka;
        }

        private void txtBayar_TextChanged(object sender, EventArgs e)
        {
            if (dgvTrans.Rows.Count > 0)
            {
                if (txtBayar.Text != "")
                {
                    int kembali = Convert.ToInt32(txtBayar.Text) - Convert.ToInt32(lblTotal.Text);
                    lblKembali.Text = kembali + "";
                    if (kembali >= 0)
                    {
                         btnCheckout.Enabled = true;
                    }
                    else
                    {
                        btnCheckout.Enabled = false;
                        lblKembali.Text = "" + 0;
                    }
                }
                else
                {
                    txtBayar.Text = "";
                    lblKembali.Text = "" + 0;
                }
            }
        }

        private void FormKasir_FormClosed(object sender, FormClosedEventArgs e)
        {
            FormLogin form = new FormLogin();
            form.MdiParent = parent;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        FormUtama parent2;
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
            parent2.showFormLogin();
        }

        private void FormKasir_Load(object sender, EventArgs e)
        {
            parent2 = (FormUtama)this.MdiParent;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            parent2.showFormRequestBarangSales();
        }
    }
}
