﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace DilerMotor
{
    public partial class FormLogin : Form
    {
        static public OracleConnection conn;
        public FormLogin()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(tbUser.Text == "owner" &&tbPass.Text == "owner")
            {
                if (comboBox1.SelectedIndex == 0)
                {
                    try
                    {
                        conn = new OracleConnection("user id=ownerORCL;password=ownerORCL;data source=orcl");
                        FormUtama.server = "ORCL";
                        conn.Open();
                        keMenuAdminGudang();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else if (comboBox1.SelectedIndex == 1)
                {
                    try
                    {
                        conn = new OracleConnection("user id=ownerDPS;password=ownerDPS;data source=dps");
                        FormUtama.server = "DPS";
                        conn.Open();
                        //ini ke menu admin dealer
                        keMenuOwner();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    try
                    {
                        conn = new OracleConnection("user id=ownerSBY;password=ownerSBY;data source=sby");
                        FormUtama.server = "SBY";
                        conn.Open();
                        //ini ke menu admin dealer
                        keMenuOwner();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
            }
            else if(tbUser.Text == "sales" && tbPass.Text == "sales")
            {
                if (comboBox1.SelectedIndex == 1)
                {
                    try
                    {
                        conn = new OracleConnection("user id=kasirDPS;password=kasirDPS;data source=dps");
                        FormUtama.server = "DPS";
                        keMenuSales(conn);
                        //conn.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else if (comboBox1.SelectedIndex == 2)
                {
                    try
                    {
                        conn = new OracleConnection("user id=kasirSBY;password=kasirSBY;data source=sby");
                        FormUtama.server = "SBY";
                        keMenuSales(conn);
                        //conn.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
                else
                {
                    MessageBox.Show("Gagal Login!");
                }
            }
            else
            {
                MessageBox.Show("Gagal Login!");
            }
        }

        FormUtama parent;
        private void FormLogin_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
        }

        private void keMenuSales(OracleConnection conn)
        {

            this.Close();
            parent.showFormKasir(conn);
        }

        private void keMenuAdminGudang()
        {
            this.Close();
            parent.showFormMenuAdmin();
        }
        private void keMenuOwner()
        {
            this.Close();
            parent.showFormRequestBarangSales();
        }
    }
}
