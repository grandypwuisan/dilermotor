﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace DilerMotor
{
    public partial class FormMasterBarang : Form
    {
        public OracleConnection conn;
        public FormMasterBarang()
        {
            InitializeComponent();
            conn = FormLogin.conn;
        }

        public FormUtama parent;
        private void FormMasterBarang_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
            refreshData();
        }

        void refreshData()
        {
            string query = "select * from barang";
            try
            {
                OracleDataAdapter oda = new OracleDataAdapter(query, conn);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "" && textBox3.Text != "")
            {
                string nama = textBox2.Text;
                string jenis = textBox3.Text;
                int hrgbeli = (int)numericUpDown1.Value;
                int hrgjual = (int)numericUpDown2.Value;
                int stok = (int)numericUpDown3.Value;
                string id = "-";
                try
                {
                    string query = "insert into barang values('" + id + "','" + nama + "', '" + jenis + "', " + hrgbeli + ","+hrgjual+","+stok+")";
                    OracleCommand cmd = new OracleCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Berhasil Insert");
                    refreshData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Field Tidak boleh Kosong");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            textBox5.Text = dataGridView1[0, row].Value.ToString();
            textBox1.Text = dataGridView1[1, row].Value.ToString();
            textBox4.Text = dataGridView1[2, row].Value.ToString();
            numericUpDown6.Value = Convert.ToInt32(dataGridView1[3, row].Value);
            numericUpDown5.Value = Convert.ToInt32(dataGridView1[4, row].Value);
            numericUpDown4.Value = Convert.ToInt32(dataGridView1[5, row].Value);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox4.Text != "")
            {
                string nama = textBox1.Text;
                string jenis = textBox4.Text;
                int hrgbeli = (int)numericUpDown6.Value;
                int hrgjual = (int)numericUpDown5.Value;
                int stok = (int)numericUpDown4.Value;
                string id = textBox5.Text;
                try
                {
                    string query = "update barang set nama_brg='" + nama + "',jenis='" + jenis + "',hrg_beli=" + hrgbeli + ",hrg_jual=" + hrgjual + ",stok=" + stok + " where id_brg='" + id + "'";
                    OracleCommand cmd = new OracleCommand(query, conn);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Berhasil Update");
                    refreshData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Field Tidak boleh Kosong");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showFormMenuAdmin();
        }
    }
}
