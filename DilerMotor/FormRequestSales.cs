﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace DilerMotor
{
    public partial class FormRequestSales : Form
    {
        OracleConnection conn = FormLogin.conn;
        OracleCommand cmd = new OracleCommand();
        OracleTransaction myTrans;
        OracleDataAdapter da = new OracleDataAdapter();
        public FormRequestSales()
        {
            InitializeComponent();
            addCMBMotor();
            setupDGVTrans();
            //request barang ke gudang
        }

        public FormUtama parent;
        private void FormRequestSales_Load(object sender, EventArgs e)
        {
            parent = (FormUtama)this.MdiParent;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            parent.showFormKasir(FormLogin.conn);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            conn.Close();
            parent.showFormLogin();
        }
        public void setupDGVTrans()
        {
            dgvTrans.Columns.Add("Kode", "Kode");
            dgvTrans.Columns.Add("Nama", "Nama");
            dgvTrans.Columns.Add("Qty", "Qty");
            dgvTrans.Columns[0].Visible = false;
        }
        public void addCMBMotor()
        {
            DataSet ds = new DataSet();
            string qry = "SELECT * FROM barang";
            OracleDataAdapter da = new OracleDataAdapter(qry, conn);
            da.Fill(ds);
            cmbMotor.DataSource = ds.Tables[0];
            cmbMotor.ValueMember = "id_brg";
            cmbMotor.DisplayMember = "nama_brg";
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            String id_brg = cmbMotor.SelectedValue.ToString();
            if (id_brg != "" && nmrQty.Value > 0)
            {
                int sama = 0;
                for (int i = 0; i < dgvTrans.Rows.Count; i++)
                {
                    if (dgvTrans.Rows[i].Cells[0].Value.ToString() == id_brg)
                    {
                        sama = 1;
                        dgvTrans.Rows[i].Cells[2].Value = nmrQty.Value;
                    }
                }
                if (sama == 0)
                {
                    dgvTrans.Rows.Add(id_brg, cmbMotor.Text, nmrQty.Value);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Yakin?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (konfirmasi == DialogResult.Yes)
            {
                if (dgvTrans.Rows.Count > 0)
                {
                    cmd.Transaction = myTrans;
                    myTrans = conn.BeginTransaction();
                    cmd.Connection = conn;
                    try
                    {
                        string nomorRequest = autoGen();
                        string now = DateTime.Now.ToString("dd-MMM-yyyy");
                        cmd.CommandText = "INSERT INTO request VALUES('" + nomorRequest + "','" + now + "','" + FormUtama.server + "','" + richTextBox1.Text + "','" + "R" + "')";
                        cmd.ExecuteNonQuery();
                        for (int i = 0; i < dgvTrans.Rows.Count; i++)
                        {
                            cmd.CommandText = "INSERT INTO dRequest VALUES('" + nomorRequest + "','" + dgvTrans[0, i].Value.ToString() + "'," + dgvTrans[2, i].Value.ToString() + ")";
                            cmd.ExecuteNonQuery();
                        }
                        myTrans.Commit();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        myTrans.Rollback();
                    }
                }
                //nmrQty.Value = 0;
                richTextBox1.Text = "";
                cmbMotor.SelectedIndex = 0;
                dgvTrans.Rows.Clear();
            }
        }

        private void DateRun_Tick(object sender, EventArgs e)
        {
            dateLabel.Text = DateTime.Now.ToLongDateString();
            timeLabel.Text = DateTime.Now.ToLongTimeString();
        }
        private String autoGen()
        {
            string now = DateTime.Now.ToString("MMyy");
            int angka = 0;
            cmd.CommandText = "Select nvl(max(substr(id_hlog,6,3)),0)+1 from request where substr(id_hlog,1,5) = 'R" + now + "'";

            angka = Convert.ToInt32(cmd.ExecuteScalar() + "");
            String str_angka = Convert.ToString(angka);
            str_angka = str_angka.PadLeft(3, '0');
            return "R" + now + str_angka;
        }

        private void dgvTrans_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvTrans.Rows.Remove(dgvTrans.Rows[e.RowIndex]);
        }

        private void dateLabel_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
            parent.showFormDaftarRequest();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
            parent.showFormDaftarTransaksi();
        }
    }
}
