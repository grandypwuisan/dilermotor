﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace DilerMotor
{
    public partial class FormUtama : Form
    {
        public static string server;
        public FormUtama()
        {
            InitializeComponent();
        }

        private void FormUtama_Load(object sender, EventArgs e)
        {
            showFormLogin();
        }
        public void showFormLogin()
        {
            FormLogin formLogin = new FormLogin();
            formLogin.MdiParent = this;
            formLogin.WindowState = FormWindowState.Maximized;
            formLogin.Show();
        }

        public void showFormMenuAdmin()
        {
            FormMenuAdmin formLogin = new FormMenuAdmin();
            formLogin.MdiParent = this;
            formLogin.WindowState = FormWindowState.Maximized;
            formLogin.Show();
        }

        public void showFormKasir(OracleConnection conn)
        {
            FormKasir formKasir = new FormKasir(conn, this);
            formKasir.MdiParent = this;
            formKasir.WindowState = FormWindowState.Maximized;
            formKasir.Show();
        }
        public void showFormMasterBarang()
        {
            FormMasterBarang formLogin = new FormMasterBarang();
            formLogin.MdiParent = this;
            formLogin.WindowState = FormWindowState.Maximized;
            formLogin.Show();
        }

        public void showFormRequestBarangSales()
        {
            FormRequestSales formLogin = new FormRequestSales();
            formLogin.MdiParent = this;
            formLogin.WindowState = FormWindowState.Maximized;
            formLogin.Show();
        }

        public void showFormAcceptRequest()
        {
            FormAcceptRequest formLogin = new FormAcceptRequest();
            formLogin.MdiParent = this;
            formLogin.WindowState = FormWindowState.Maximized;
            formLogin.Show();
        }

        public void showFormDaftarRequest()
        {
            FormDaftarRequest formLogin = new FormDaftarRequest();
            formLogin.MdiParent = this;
            formLogin.WindowState = FormWindowState.Maximized;
            formLogin.Show();
        }

        public void showFormDaftarTransaksi()
        {
            FormDaftarTrans formLogin = new FormDaftarTrans();
            formLogin.MdiParent = this;
            formLogin.WindowState = FormWindowState.Maximized;
            formLogin.Show();
        }
    }
}
