--ORCL
/*SISTEM*/
CREATE USER ownerORCL IDENTIFIED BY ownerORCL;

grant all privileges to ownerORCL;

conn ownerORCL@ORCL/ownerORCL;
drop table barang cascade constraint purge;
drop table hLogBrg cascade constraint purge;
drop table dLogBrg cascade constraint purge;

CREATE PUBLIC DATABASE LINK linkKeOwnerDPS CONNECT TO ownerDPS IDENTIFIED BY ownerDPS USING 'keDps';
CREATE PUBLIC DATABASE LINK linkKeOwnerSBY CONNECT TO ownerSBY IDENTIFIED BY ownerSBY USING 'keSby';

create or replace synonym hTransSBY for hTrans@linkKeOwnerSBY; 
create or replace synonym dTransSBY for dTrans@linkKeOwnerSBY;
create or replace synonym hTransDPS for hTrans@linkKeOwnerDPS;
create or replace synonym dTransDPS for dTrans@linkKeOwnerDPS;

--owner
create table barang(
    id_brg VARCHAR2(5) primary key,
    nama_brg VARCHAR2(50),
    jenis VARCHAR2(50),
    hrg_beli number(10),
    hrg_jual number(10),
    stok number (10)
);
--contoh insert
insert into barang values('','CBR JB99','Sport',1000,10000,10);
insert into barang values('','Mio','Bebek',500,5000,10);

drop sequence sq_logBarang;
create sequence sq_logBarang
minvalue 1;

drop table logBarang cascade constraint purge;
create table logBarang (
  log_no number primary key,
  change_date date,
  dml_type varchar2(1),
  id_brg varchar2(5),
  flag varchar2(1)
);

create table hLogBrg(
    id_hlog varchar2(5) primary key,
    tanggal date,
    tujuan varchar(5),
    keterangan varchar(50),
    status_brg varchar2(1)
);

create table dLogBrg(
    id_hlog varchar2(5),
    id_brg varchar2(5),
    qty number
);

--autogen id barang
drop function fGenKdBrg;
create or replace function fGenKdBrg
return varchar2
is
  tempKode barang.id_brg%type;
  ctr number;
begin
    select count(*) into ctr 
    from barang;
    IF ctr = 0
    THEN tempKode := 'M0001';
    ELSE
    select 'M'||lpad(to_number(max(substr(id_brg,2,4)))+1,4,'0') into tempKode
    from barang;
    END IF;
  
  return tempKode;
end;
/
show err;

drop trigger tLogBarang;
create or replace trigger tLogBarang
before insert or update or delete
on barang
for each row
begin
  if inserting then
    :new.id_brg := fGenKdBrg;
    insert into logBarang values
	(sq_logBarang.nextval, sysdate, 'I', :new.id_brg, 'F');
    --buat untuk nsert masing-masing db yang ada tapi stok 0
    insert into barang@linkKeOwnerDPS values(:new.id_brg,:new.nama_brg,:new.jenis,:new.hrg_beli,:new.hrg_jual,0);
    insert into barang@linkKeOwnerSBY values(:new.id_brg,:new.nama_brg,:new.jenis,:new.hrg_beli,:new.hrg_jual,0);
  elsif updating then
    insert into logBarang values
	(sq_logBarang.nextval, sysdate, 'U', :new.id_brg, 'F');
    --update juga di dps dan sby
    update barang@linkKeOwnerDPS set nama_brg=:new.nama_brg,jenis=:new.jenis,hrg_beli= :new.hrg_beli,
    hrg_jual = :new.hrg_jual where id_brg = :old.id_brg;
    update barang@linkKeOwnerSBY set nama_brg=:new.nama_brg,jenis=:new.jenis,hrg_beli= :new.hrg_beli,
    hrg_jual = :new.hrg_jual where id_brg = :old.id_brg;
  else --delete
    insert into logBarang values
	(sq_logBarang.nextval, sysdate, 'D', :old.id_brg, 'F');
  end if;
end;
/
show err;

create or replace trigger tReqBrg
before update
on hLogBrg
for each row
declare
    tStatus varchar2(5);
begin
    tStatus := :new.status_brg;
    IF tStatus = 'A'
    THEN
    for i in (select * from dLogBrg where id_hlog = :old.id_hlog)
    LOOP
        update barang set stok = stok - i.qty where id_brg = i.id_brg;
        IF :old.tujuan = 'DPS'
        THEN
            update barang@linkKeOwnerDPS set stok = stok + i.qty where id_brg = i.id_brg;
        ELSE
            update barang@linkKeOwnerSBY set stok = stok + i.qty where id_brg = i.id_brg;
        END IF;
    end LOOP;
    END IF;
end;
/
show err;