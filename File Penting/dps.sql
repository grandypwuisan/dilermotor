--DPS
/*SISTEM*/
-- ini yang dianggap sales ya !!!
CREATE USER kasirDPS IDENTIFIED BY kasirDPS;
CREATE USER ownerDPS IDENTIFIED BY ownerDPS;

grant all privileges to ownerDPS;

conn ownerDPS@DPS/ownerDPS;

create public DATABASE link linkKeOwnerORCL connect to ownerORCL identified by ownerORCL USING 'keOrcl';

--kalau mau request barang 
create or replace synonym request for hLogBrg@linkKeOwnerORCL;
--kalau mau liat detail request
create or replace synonym dRequest for dLogBrg@linkKeOwnerORCL;

drop table barang cascade constraint purge;
drop table hTrans cascade constraint purge;
drop table dTrans cascade constraint purge;
drop table hLogBrg cascade constraint purge;
drop table dLogBrg cascade constraint purge;

--owner
create table barang(
    id_brg VARCHAR2(5) primary key,
    nama_brg VARCHAR2(50),
    jenis VARCHAR2(50),
    hrg_beli number(10),
    hrg_jual number(10),
    stok number (10)
);

create table hTrans(
    --hhmmyy + urut
    idTrans VARCHAR2(10) primary key,
    tglTrans date,
    grandTotal number,
    kostumer VARCHAR2(30),
    catatan VARCHAR2(50)
);

create table dTrans(
    idTrans VARCHAR2(10),
    id_brg VARCHAR2(5),
    qty number,
    subTotal number
);

/*
insert into hTrans values('T151219001',to_date('10/03/2017','dd-mm-yyyy'),1000,'Paijo','-');
insert into hTrans values('','',1000,'Paijo','-');

drop function fGenKdTrans;
drop trigger thTrans;

create or replace function fGenKdTrans
return varchar2
is
  tempKode hTrans.idTrans%type;
  ctr number;
begin
    select 'T'||to_char(sysdate,'DDMMYY')||lpad(to_number(nvl(max(substr(idTrans,8,3)),0))+1,3,'0') into tempKode
    from hTrans
    where substr(idTrans,1,7)='T'||to_char(sysdate,'DDMMYY');
    return tempKode;
end;
/
show err;


create or replace trigger thTrans
before insert
on hTrans
for each row
begin
  if inserting then
    :new.idTrans := fGenKdTrans;
    :new.tglTrans := sysdate;
  end if;
end;
/
show err;
*/

grant connect to kasirDPS;
grant create synonym to kasirDPS;
grant select, update on ownerDPS.Barang to kasirDPS;
grant insert, select on ownerDPS.hTrans to kasirDPS;
grant insert, select on ownerDPS.dTrans to kasirDPS;

conn kasirDPS@DPS/kasirDPS;

update barang set stok = 3 where id_brg= 'M0001';
update barang set stok = 5 where id_brg= 'M0002';

drop synonym barang;
drop synonym htrans;
drop synonym dtrans;

create or replace synonym barang for ownerDPS.Barang;
create or replace synonym htrans for ownerDPS.htrans;
create or replace synonym dtrans for ownerDPS.dtrans;