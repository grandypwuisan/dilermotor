/*SISTEM*/
CREATE USER kasirSBY IDENTIFIED BY kasirSBY;
CREATE USER ownerSBY IDENTIFIED BY ownerSBY;

grant all privileges to ownerSBY;
conn ownerSBY@SBY/ownerSBY;

create public DATABASE link linkKeOwnerORCL connect to ownerORCL identified by ownerORCL USING 'keOrcl';

--kalau mau request barang 
create or replace synonym request for hLogBrg@linkKeOwnerORCL;
--kalau mau liat detail request
create or replace synonym dRequest for dLogBrg@linkKeOwnerORCL;

drop table barang cascade constraint purge;
drop table hLogBrg cascade constraint purge;
drop table dLogBrg cascade constraint purge;

--owner
create table barang(
    id_brg VARCHAR2(5) primary key,
    nama_brg VARCHAR2(50),
    jenis VARCHAR2(50),
    hrg_beli number(10),
    hrg_jual number(10),
    stok number (10)
);

create table hTrans(
    --hhmmyy + urut
    idTrans VARCHAR2(10) primary key,
    tglTrans date,
    grandTotal number,
    kostumer VARCHAR2(30),
    catatan VARCHAR2(50)
);

create table dTrans(
    idTrans VARCHAR2(10),
    id_brg VARCHAR2(5),
    qty number,
    subTotal number
);

grant connect to kasirSBY;
grant create synonym to kasirSBY;
grant select, update on ownerSBY.Barang to kasirSBY;
grant insert, select on ownerSBY.hTrans to kasirSBY;
grant insert, select on ownerSBY.dTrans to kasirSBY;

conn kasirSBY@SBY/kasirSBY;
drop synonym barang;
drop synonym htrans;
drop synonym dtrans;

create or replace synonym barang for ownerSBY.Barang;
create or replace synonym htrans for ownerSBY.htrans;
create or replace synonym dtrans for ownerSBY.dtrans;
